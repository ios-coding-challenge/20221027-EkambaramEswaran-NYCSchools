//
//  Constant.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import Foundation

//MARK: List of API service calls
struct NYCSchoolAPIEndpoints {
    static let schoolsAPI = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let schooldetailsAPI = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
