//
//  NYCSchoolDetailViewModel.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import Foundation
import NetworkManager

//MARK: ViewModel class for Detail screen
class NYCSchoolDetailViewModel: NSObject {
    
    private var apiService : NYCSchoolAPIService!
    var schoolId: String?
   
    private(set) var schoolDetails : [NYCSchoolDetailModel]! {
        didSet {
            self.bindNYCSchoolDetailViewModelToController()
        }
    }
    
    var bindNYCSchoolDetailViewModelToController : (() -> ()) = {}
    
    init(schoolId: String) {
        super.init()
        self.schoolId = schoolId
        self.apiService = NYCSchoolAPIService()
        callFuncToGetSchoolDetailData()
    }

    func callFuncToGetSchoolDetailData() {
        
        self.apiService.request([NYCSchoolDetailModel].self, from: NYCSchoolAPIEndpoints.schooldetailsAPI + (schoolId ?? "")) { [weak self] result in
            switch result {
            case .success(let response):
                self?.schoolDetails = response
                // failure case
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
