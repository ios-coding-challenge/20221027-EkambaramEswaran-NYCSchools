//
//  NYCSchoolDetailViewController.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import UIKit

class NYCSchoolDetailViewController: UIViewController {

    var selectedSchool: NYCSchoolHomeModel?
    private var nycSchoolDetailViewModel: NYCSchoolDetailViewModel!
    var activityView : UIActivityIndicatorView?
    
    @IBOutlet weak var numSatTakersLabel: UILabel!
    @IBOutlet weak var critialReadinfAvgLabel: UILabel!
    @IBOutlet weak var mathAvgLabel: UILabel!
    @IBOutlet weak var writingAvgScoreLabel: UILabel!
    @IBOutlet weak var schoolDetailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedSchool?.schoolName
        callToViewModelForUIUpdate()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func callToViewModelForUIUpdate(){
        showActivityIndicator()
        self.nycSchoolDetailViewModel = NYCSchoolDetailViewModel(schoolId: selectedSchool?.dbn ?? "")
        self.nycSchoolDetailViewModel.bindNYCSchoolDetailViewModelToController = {
            self.updateDataSource()
        }
    }

    //MARK: Update the UI once serice call is completed 
    func updateDataSource(){
        DispatchQueue.main.async {
            if let schoolDetail = self.nycSchoolDetailViewModel.schoolDetails.first {
                self.numSatTakersLabel.text = schoolDetail.numOfSatTestTakers
                self.critialReadinfAvgLabel.text = schoolDetail.satCriticalReadingAvgScore
                self.mathAvgLabel.text = schoolDetail.satMathAvgScore
                self.writingAvgScoreLabel.text = schoolDetail.satWritingAvgScore
            }
            self.schoolDetailLabel.text
            = self.selectedSchool?.overveiwParagraph
            self.hideActivityIndicator()
        }
    }
}


//TODO: Due to time constaint duplicate code added here
extension NYCSchoolDetailViewController {
   
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView ?? UIActivityIndicatorView())
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityView?.stopAnimating()
    }
}
