//
//  File.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

//MARK: Schools list model for Home screen
struct NYCSchoolHomeModel: Codable {
    
    let dbn, schoolName, primaryAddressLine1, city: String?
    let zip, stateCode: String?
    let overveiwParagraph : String?
    
      enum CodingKeys: String, CodingKey {
          case dbn
          case schoolName = "school_name"
          case primaryAddressLine1 = "primary_address_line_1"
          case city, zip
          case stateCode = "state_code"
          case overveiwParagraph = "overview_paragraph"
      }
}
