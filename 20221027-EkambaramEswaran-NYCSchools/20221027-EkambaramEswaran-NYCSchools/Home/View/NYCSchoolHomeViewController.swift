//
//  ViewController.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import UIKit

class NYCSchoolHomeViewController: UIViewController {
    
    
    @IBOutlet weak var schoolsTableView: UITableView!
    private var nycSchoolHomeViewModel: NYCSchoolHomeViewModel!
    private var dataSource: NYCSchoolHomeTableViewDataSource<NYCSchoolHomeTableViewCell, NYCSchoolHomeModel>!
    var activityView : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callToViewModelForUIUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func callToViewModelForUIUpdate(){
        showActivityIndicator()
        self.nycSchoolHomeViewModel = NYCSchoolHomeViewModel()
        self.nycSchoolHomeViewModel.bindNYCSchoolHomeViewModelToController = {
            self.updateDataSource()
        }
    }
    
    //MARK: Update the UI once serice call is completed 
    func updateDataSource(){
        
        self.dataSource = NYCSchoolHomeTableViewDataSource(cellIdentifier: "NYCSchoolHomeTableViewCell", schools: self.nycSchoolHomeViewModel.schoolsData, homeVC: self, configureCell: { (cell, school) in
            
            if let schoolCode = school.dbn, let schoolName = school.schoolName {
                cell.schoolName.text = schoolCode + " | " + schoolName
            } else {
                //
            }
            
            if let primaryAdress = school.primaryAddressLine1,
                  let stateCode = school.stateCode,
               let zip = school.zip {
                cell.schoolAddress.text =  "\(primaryAdress), \(stateCode), \(zip)"
            } else {
                //
            }
        })
        
        DispatchQueue.main.async {
            self.schoolsTableView.dataSource = self.dataSource
            self.schoolsTableView.delegate = self.dataSource
            self.schoolsTableView.reloadData()
            self.hideActivityIndicator()
        }
    }
}

//TODO: Due to time constaint duplicate code added here
extension NYCSchoolHomeViewController {
    
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView ?? UIActivityIndicatorView())
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityView?.stopAnimating()
    }
}
