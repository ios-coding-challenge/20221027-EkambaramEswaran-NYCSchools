//
//  NYCSchoolHomeTableViewCell.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import UIKit

//MARK: Home screen list of school cell
class NYCSchoolHomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
