//
//  NYCSchoolHomeTableViewDataSource.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import Foundation
import UIKit

//MARK: Home screen UITableViewDataSource and UITableViewDelegate
class NYCSchoolHomeTableViewDataSource<CELL : UITableViewCell, T> : NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private var cellIdentifier : String!
    private var schools : [T]!
    var configureCell : (CELL, T) -> () = {_,_ in }
    var homeVC: UIViewController!
    
    
    init(cellIdentifier : String, schools : [T], homeVC: UIViewController, configureCell : @escaping (CELL, T) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.schools =  schools
        self.configureCell = configureCell
        self.homeVC = homeVC
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CELL
        let item = self.schools[indexPath.row]
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.systemGray6 : .white
        self.configureCell(cell, item)
        return cell
    }
    
    
    //TODO: Due to time constaint added delegate method here 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        guard let detailViewController = storyBoard.instantiateViewController(
            withIdentifier: "NYCSchoolDetailViewController") as? NYCSchoolDetailViewController
        else { return }
        detailViewController.selectedSchool = self.schools[indexPath.row] as? NYCSchoolHomeModel
        self.homeVC.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.homeVC.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
