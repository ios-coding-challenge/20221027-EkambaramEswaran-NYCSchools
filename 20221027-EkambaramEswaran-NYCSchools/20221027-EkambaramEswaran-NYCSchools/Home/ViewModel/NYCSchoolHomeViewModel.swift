//
//  NYCSchoolHomeViewModel.swift
//  20221027-EkambaramEswaran-NYCSchools
//
//  Created by Ekambaram E on 10/27/22.
//

import Foundation
import NetworkManager

//MARK: ViewModel class for Home screen
class NYCSchoolHomeViewModel: NSObject {
    
    private var apiService : NYCSchoolAPIService!
   
    private(set) var schoolsData : [NYCSchoolHomeModel]! {
        didSet {
            self.bindNYCSchoolHomeViewModelToController()
        }
    }
    
    var bindNYCSchoolHomeViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.apiService = NYCSchoolAPIService()
        callFuncToGetSchoolsData()
    }
    
    //MARK: Making service call for Home screen | getting list of school 
    func callFuncToGetSchoolsData() {
        self.apiService.request([NYCSchoolHomeModel].self, from: NYCSchoolAPIEndpoints.schoolsAPI) { [weak self] result in
            switch result {
            case .success(let response):
                self?.schoolsData = response
                // failure case
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

