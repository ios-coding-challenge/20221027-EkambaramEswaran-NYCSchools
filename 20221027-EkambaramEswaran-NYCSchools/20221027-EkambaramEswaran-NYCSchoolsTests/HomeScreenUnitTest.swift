//
//  HomeScreenUnitTest.swift
//  20221027-EkambaramEswaran-NYCSchoolsTests
//
//  Created by Ekambaram E on 10/27/22.
//

import XCTest
@testable import _0221027_EkambaramEswaran_NYCSchools

final class HomeScreenUnitTest: XCTestCase {
    
    var homeVC: NYCSchoolHomeViewController!
    private var NYCSchoolHomeViewModel: NYCSchoolHomeViewModel!
    private var dataSource: NYCSchoolHomeTableViewDataSource<NYCSchoolHomeTableViewCell, NYCSchoolHomeModel>!

    override func setUp() {
        super.setUp()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let detailVC = storyBoard.instantiateViewController(withIdentifier: "NYCSchoolHomeViewController") as! NYCSchoolHomeViewController
        homeVC = detailVC
        homeVC.loadViewIfNeeded()
        homeVC.beginAppearanceTransition(true, animated: true)
        homeVC.endAppearanceTransition()
    }

    func testCallToViewModelForUIUpdate() {
        homeVC.callToViewModelForUIUpdate()
        homeVC.hideActivityIndicator()
        homeVC.showActivityIndicator()
        homeVC.schoolsTableView.dataSource = self.dataSource
        homeVC.schoolsTableView.delegate = self.dataSource
        homeVC.schoolsTableView.reloadData()
        XCTAssert(true)
        //homeVC.updateDataSource()
    }
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
