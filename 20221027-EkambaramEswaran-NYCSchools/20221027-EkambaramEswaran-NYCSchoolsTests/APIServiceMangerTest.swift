//
//  APIServiceMangerTest.swift
//  20221027-EkambaramEswaran-NYCSchoolsTests
//
//  Created by Ekambaram E on 10/27/22.
//

import XCTest
@testable import _0221027_EkambaramEswaran_NYCSchools

final class APIServiceMangerTest: XCTestCase {

    private var homeViewModel : NYCSchoolHomeViewModel!
    
    override func setUp() {
        super.setUp()
        self.homeViewModel = NYCSchoolHomeViewModel()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }


    //TODO: Dependency Injection is pending - Mock local json respone.
    func testAPIService() throws {
        self.homeViewModel.callFuncToGetSchoolsData()
        XCTAssert(true)
    }

    func testAPIError() {
        XCTAssertEqual("Bad url", NYCSchoolNetworkError.badURL.errorDescription)
    }

}
