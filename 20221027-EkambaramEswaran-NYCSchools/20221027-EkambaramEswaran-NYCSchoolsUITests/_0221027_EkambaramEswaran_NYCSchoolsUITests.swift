//
//  _0221027_EkambaramEswaran_NYCSchoolsUITests.swift
//  20221027-EkambaramEswaran-NYCSchoolsUITests
//
//  Created by Ekambaram E on 10/27/22.
//

import XCTest

final class _0221027_EkambaramEswaran_NYCSchoolsUITests: NYCSchoolBaseUITest {

    var selectedSchoolName: String?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    func testNavigateDetailsHomeScreen01() throws {
        // UI tests must launch the application that they test.
        app.launch()
        if (app.tables.children(matching: .cell).element(boundBy: 0).waitForExistence(timeout: 10)) {
            app.tables.children(matching: .cell).element(boundBy: 3).tap()
        }
        
//        if (app.tables.cells.containing(.staticText, identifier:"17K548 | Brooklyn School for Music & Theatre").staticTexts["883 Classon Avenue, NY, 11225"].exists) {
//            app.tables.cells.containing(.staticText, identifier:"17K548 | Brooklyn School for Music & Theatre").staticTexts["883 Classon Avenue, NY, 11225"].swipeUp()
//        }
        
        if (app.navigationBars["Brooklyn School for Music & Theatre"].staticTexts["Brooklyn School for Music & Theatre"].waitForExistence(timeout: 5)) {
            app.navigationBars["Brooklyn School for Music & Theatre"].staticTexts["Brooklyn School for Music & Theatre"].tap()
            XCTAssert(true)
        }
    }
}
